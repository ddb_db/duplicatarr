module gitlab.com/ddb_db/duplicatarr

go 1.21.5

require (
	github.com/caarlos0/env/v10 v10.0.0
	github.com/otiai10/copy v1.14.0
	github.com/vargspjut/wlog v1.0.11
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/stretchr/testify v1.8.4
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
