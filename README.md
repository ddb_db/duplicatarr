# duplicatarr
A "custom script" event handler for Sonarr that will duplicate series imports to a secondary location.  Duplication to multiple targets is on the roadmap, but not
yet supported.  This tool deals with an issue around **automated deleting of media**.  If you never delete imported media, this tool probably won't serve you any
purpose.

## User Personas
A quick description of the people involved in this scenario:

**Alvin:** The media server administrator.  Alvin operates and maintains Sonarr, Emby, the NAS, everything.  Everyone else here gets to watch their favourite content
because of Alvin.

**Simon:** A user of Emby.  Simon likes to watch some movies and tv shows.  The tv shows are imported via Sonarr.

**Theo:** Another user of Emby.  Theo also likes to watch various movies and tv shows.  The tv shows are imported via Sonarr.

## Preview Release
Alvin is using this tool and it's working nicely for his use case.  But your setup may not be the same as Alvin's and so something unexpected might happen if you deploy this
tool in your environment.  Take suitable precautions before using this tool!  But if you do use it, please feel free to add feature requests, bugs, etc. to the project tracker.

## Use Case/Problem Being Solved
### Background
**Alvin's** NAS does not have a lot of space.  Enough, but it's not unlimited.  Because of this, Alvin runs some automated scripts that delete watched tv episodes
a few days after they've been marked as watched in Emby.  Users have the option to mark shows/episodes as favourites in Emby, which will prevent the automated
deletion.  Anything not marked as a fav is deleted a few days after it's been watched.  This prevents Alvin from having to constantly add space to the NAS.  Alvin
likes this.  Simon and Theo don't mind it either because they don't watch most things more than once.  Classic shows, etc. that people want to watch repeatedly are
saved in a separate library within Emby and are never deleted.  "Classic" shows can be kept around because space isn't being consumed by shows that Simon and Theo
watch once and never watch again.  This is all good and keeps Alvin's costs down by not having to feed the NAS a new 20TB hard drive every year or two.

Every user is granted access to a common set of libraries (classic tv, all movies, etc.).  Each user also has their own library where their specific tv shows are stored.
There is rarely overlap between the shows that Simon and Theo are interested in.  This is good and makes the automated deletion pretty easy and effective.

### The Problem
But... what happens when Simon and Theo are interested in the same show?  Alvin could put the show in a common library for them to both access.  The problem here is now
the automated deletion will likely delete episodes after one has watched it but not the other.  That will get some angry text messages.  The other option is to duplicate
the series in each of their user libraries.  Now when one of them watches an episode it marks their copy as watched and is then deleted while the other is left untouched.

This is the solution Alvin has chosen.  And this tool is what duplicates the series from Sonarr.  By tagging a series with a well defined tag, this tool will duplicate all
imported files for that series into the target.  Now episodes are stored for each user individually and will stick around until that user watches their copy of
the episode.

But you might ask, "Alvin!  You're making multiple copies of episodes, doesn't that defeat the purpose of auto deleting stuff and keeping disk usage down?"  Well, yes,
sort of.  First, this is rare.  So it's not like every episode of every series is being duplicated.  But it happens just enough that Alvin needed a solution.  The space
that is lost due to duplication is made up for in the amount of space saved by deleting episodes as they are watched.  The vast majority of **tv episodes** watched by Simon
and Theo are watched once and never again.  Once Alvin established that pattern of behaviour he was able to setup the automated episode deletion from Emby.  This
recovered **plenty** of space.  More than enough to cover the handful of series that are being duplicated.  Once all users watch their copy of the episode, it's deleted and
that space for duplication is recovered.

### The Alternative
The alternative would have been to modify the auto deletion tooling to handle the case where multiple users had to watch an episode before deleting it.  This was possible,
but as mentioned above, the way (this installation of) Emby is setup is that each user has their own tv libraries where they go to watch their shows.  Having common
libraries for certain shows (other than classics) would have been confusing and Alvin didn't want to deal with that.  Duplicating series imports provides the best user
experience.

## The Solution: duplicatarr
Simon and Theo both watch "XYZ Show".  Alvin has setup Sonarr to import episodes into `/tv/theo/XYZ Show/` for Theo.  This works great and Theo gets his episodes as they
become available.  To get a copy of imported episodes into Simon's library, Alvin adds a special tag to the series in Sonarr: `duplicatarr-simon-theo`.  With that tag,
and duplicatarr configured to receive import and upgrade events, it will now duplicate all imported episodes (including upgrades of episdoes) into Simon's library.  Now
both Simon and Theo can watch the show and as they watch their copy of the episodes, only their copy will be automatically deleted after it's been watched.

This works because of the paths used to store the media:

`/tv/theo/XYZ Show/`
`/tv/simon/XYZ Show/`

In this first version of the tool, the two values on the end of the tag are used for a simple find and replace in the file paths.  So replace the **first occurence** of `theo`
with `simon` in the imported file path and make a copy at that location.  In the case of an upgrade, replace files as needed.  If your media paths can't easily be modifed as
described here then this version of the tool may not work for you.  A future version may rethink the format of the tag in order to be more flexible.

## Roadmap
* A few more tests are needed to help validate correctness.
* Automated builds would be nice.
* Currently, only one target destination can be specified in the tag.  Sometimes more than two people are interested in the same series so multiple targets should be supported.
* Sonarr can alert Emby when something is imported.  Duplicates made by duplicatarr do not alert Emby.  Duplicates won't be visible until Emby's next periodic media scan.
  * Enhance duplicatarr to send signals to Emby (and possibly other systems, but Alvin uses Emby so that will be priority).
