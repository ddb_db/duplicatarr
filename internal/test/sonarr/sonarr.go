package sonarr

import "strings"

func NewEnv(isUpgrade bool, episodeFilePath string, seriesTags, deletedPaths []string) map[string]string {
	upgrade := "False"
	if isUpgrade {
		upgrade = "True"
	}
	env := map[string]string{
		"sonarr_episodefile_path": episodeFilePath,
		"sonarr_series_tags":      strings.Join(seriesTags, "|"),
		"sonarr_eventtype":        "Download",
		"sonarr_isupgrade":        upgrade,
	}
	if len(deletedPaths) > 0 {
		env["sonarr_deletedpaths"] = strings.Join(deletedPaths, "|")
	}
	return env
}
