package fs

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

type Fs struct {
	Root string
}

func New(dstRoot string, dstFiles, srcFiles []string) *Fs {
	fs := &Fs{}
	fs.init(dstRoot, dstFiles, srcFiles)
	return fs
}

func (fs *Fs) init(dstRoot string, dstFiles, srcFiles []string) {
	tmpRoot, err := os.MkdirTemp("", "duplicatarr_test_*")
	if err != nil {
		panic(err)
	}
	fs.Root = tmpRoot

	for _, f := range dstFiles {
		if err := createFile(tmpRoot, f, '0'); err != nil {
			panic(err)
		}
	}

	for _, f := range srcFiles {
		if err := createFile(tmpRoot, f, '1'); err != nil {
			panic(err)
		}
	}

	if err := os.MkdirAll(fmt.Sprintf("%s%c%s", tmpRoot, os.PathSeparator, filepath.Clean(dstRoot)), 0700); err != nil {
		panic(err)
	}
}

func (fs *Fs) Destroy() error {
	return os.RemoveAll(fs.Root)
}

func (fs *Fs) FileExists(path string) bool {
	info, err := os.Stat(filepath.Clean(fmt.Sprintf("%s/%s", fs.Root, path)))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return false
		}
		panic(err)
	}
	return info.Mode().IsRegular()
}

func (fs *Fs) FullPaths(paths []string) []string {
	result := make([]string, 0, len(paths))
	for _, p := range paths {
		result = append(result, fs.FullPath(p))
	}
	return result
}

func (fs *Fs) FullPath(path string) string {
	return filepath.Clean(fmt.Sprintf("%s/%s", fs.Root, path))
}

func (fs *Fs) FileReplaced(path string) bool {
	if !fs.FileExists(path) {
		return false
	}
	return isContentsEqual(filepath.Clean(fmt.Sprintf("%s/%s", fs.Root, path)), '1')
}

func (fs *Fs) FileUnmodified(path string) bool {
	if !fs.FileExists(path) {
		return false
	}
	return isContentsEqual(filepath.Clean(fmt.Sprintf("%s/%s", fs.Root, path)), '0')
}

func isContentsEqual(fullPath string, expected rune) bool {
	contents, err := os.ReadFile(filepath.Clean(fullPath))
	if err != nil {
		panic(err)
	}
	return len(contents) == 1 && byte(expected) == contents[0]
}

func createFile(root, f string, content rune) error {
	cf := filepath.Clean(fmt.Sprintf("%s%c%s", root, os.PathSeparator, f))
	if err := os.MkdirAll(filepath.Dir(cf), 0700); err != nil {
		return err
	}
	return os.WriteFile(cf, []byte{byte(content)}, 0600)
}
