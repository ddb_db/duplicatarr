package parser

import (
	"fmt"

	"github.com/caarlos0/env/v10"
)

type Parser interface {
	Parse() (Event, error)
}

type EventType int

type Event struct {
	Tags             []string `env:"sonarr_series_tags" envSeparator:"|"`
	EpisodeFilePath  string   `env:"sonarr_episodefile_path,notEmpty"`
	DeletedFilePaths []string `env:"sonarr_deletedpaths" envSeparator:"|"`
	IsUpgrade        bool     `env:"sonarr_isupgrade"`
}

func New() Parser {
	return defaultImpl{}
}

func NewWithEnv(e map[string]string) Parser {
	return mockImpl{
		env: e,
	}
}

type defaultImpl struct{}

func (impl defaultImpl) Parse() (Event, error) {
	result := Event{}
	var err error
	if err = env.Parse(&result); err != nil {
		err = fmt.Errorf("parser: input error: %w", err)
	}
	return result, err
}

type mockImpl struct {
	env map[string]string
}

func (impl mockImpl) Parse() (Event, error) {
	result := Event{}
	var err error
	if err = env.ParseWithOptions(&result, env.Options{Environment: impl.env}); err != nil {
		err = fmt.Errorf("parser: input error: %w", err)
	}
	return result, err
}
