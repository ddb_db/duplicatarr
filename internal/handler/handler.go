package handler

import (
	"errors"
	"fmt"
	"os"
	"strings"

	cp "github.com/otiai10/copy"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/duplicatarr/internal/parser"
)

const TagSeparator = "-"
const TagPrefix = "duplicatarr" + TagSeparator

type Handler interface {
	Run(parser.Event) error
}

type action struct {
	sourceFile   string
	targetFile   string
	deletedFiles []string
}

func New() Handler {
	return defaultImpl{
		pretend: false,
	}
}

func NewDryRun() Handler {
	return defaultImpl{
		pretend: true,
	}
}

type defaultImpl struct {
	pretend bool
}

func (impl defaultImpl) Run(e parser.Event) error {
	for _, t := range e.Tags {
		a := parseAction(t, e.EpisodeFilePath, e.DeletedFilePaths)
		if a == nil {
			continue
		}
		if err := impl.execute(*a, e.IsUpgrade); err != nil {
			return fmt.Errorf("handler: action failed: %w", err)
		}
	}
	return nil
}

func fileExists(path string) bool {
	f, err := os.Stat(path)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			wlog.Warningf("handler: stat error: %s", err.Error())
		}
		return false
	}
	return f.Mode().IsRegular()
}

func (impl defaultImpl) execute(a action, isUpgrade bool) error {
	if isUpgrade {
		found := false
		candidates := make([]string, 0, len(a.deletedFiles)+1)
		candidates = append(candidates, a.deletedFiles...)
		candidates = append(candidates, a.targetFile)
		for _, f := range candidates {
			found = fileExists(f)
			if found {
				break
			}
		}
		if !found {
			wlog.Warningf("handler: upgrade for [%s] ignored, no replacable target found", a.targetFile)
			return nil
		}
	}

	for _, f := range a.deletedFiles {
		if f == a.targetFile {
			wlog.Debugf("handler: skipped delete; replacing instead [%s]", f)
			continue
		}

		if !impl.pretend {
			if err := os.Remove(f); err != nil {
				wlog.Warningf("handler: file delete failed [%s]: %s", f, err.Error())
			} else {
				wlog.Infof("handler: file deleted [%s]", f)
			}
		} else {
			wlog.Infof("handler: dry-run, skipped delete [%s]", f)
		}
	}

	if !impl.pretend {
		if err := cp.Copy(a.sourceFile, a.targetFile); err != nil {
			return err
		}
		wlog.Infof("handler: copied [%s] to [%s]", a.sourceFile, a.targetFile)
	} else {
		wlog.Infof("handler: dry-run, skipped copy of [%s] to [%s]", a.sourceFile, a.targetFile)
	}
	return nil
}

func parseAction(tag, srcFile string, deletedFiles []string) *action {
	if !strings.HasPrefix(tag, TagPrefix) {
		wlog.Debugf("handler: tag ignored [%s]", tag)
		return nil
	}
	fields := strings.SplitN(tag, TagSeparator, 3)

	a := action{
		sourceFile: srcFile,
		targetFile: strings.Replace(srcFile, fields[2], fields[1], 1),
	}
	a.deletedFiles = make([]string, 0, len(deletedFiles))
	for _, d := range deletedFiles {
		a.deletedFiles = append(a.deletedFiles, strings.Replace(d, fields[2], fields[1], 1))
	}
	wlog.Debugf("handler: action parsed: %+v", a)
	return &a
}
