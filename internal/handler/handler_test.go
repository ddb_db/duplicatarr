package handler_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/duplicatarr/internal/handler"
	"gitlab.com/ddb_db/duplicatarr/internal/parser"
	"gitlab.com/ddb_db/duplicatarr/internal/test/fs"
	"gitlab.com/ddb_db/duplicatarr/internal/test/sonarr"
)

func TestDownloadCopiesToDest(t *testing.T) {
	const dstRoot = "dst/series"
	const srcMkv = "src/series/S01E01.mkv"
	const dstMkv = "dst/series/S01E01.mkv"

	fsRoot := fs.New(dstRoot, nil, []string{srcMkv})
	defer fsRoot.Destroy()

	e, err := parser.NewWithEnv(sonarr.NewEnv(false, fsRoot.FullPath(srcMkv), []string{fmt.Sprintf("%sdst%ssrc", handler.TagPrefix, handler.TagSeparator)}, nil)).Parse()
	if err != nil {
		panic(err)
	}

	sut := handler.New()
	if err := sut.Run(e); err != nil {
		panic(err)
	}
	assert.FileExists(t, fsRoot.FullPath(dstMkv))
	assert.True(t, fsRoot.FileReplaced(dstMkv))
}

func TestDownloadDeletesExpectedDestFiles(t *testing.T) {
	const dstRoot = "dst/series"
	const srcMkv = "src/series/S01E01.mkv"
	const dstMkv = "dst/series/S01E01.mkv"
	dstExisting := []string{"dst/series/old1.mkv", "dst/series/old2.mkv"}
	srcReplacing := []string{"src/series/old1.mkv", "src/series/old2.mkv"}

	fsRoot := fs.New(dstRoot, dstExisting, []string{srcMkv})
	defer fsRoot.Destroy()

	e, err := parser.NewWithEnv(
		sonarr.NewEnv(false, fsRoot.FullPath(srcMkv), []string{fmt.Sprintf("%sdst%ssrc", handler.TagPrefix, handler.TagSeparator)}, fsRoot.FullPaths(srcReplacing)),
	).Parse()
	if err != nil {
		panic(err)
	}

	for _, f := range dstExisting {
		assert.True(t, fsRoot.FileUnmodified(f))
	}

	sut := handler.New()
	if err := sut.Run(e); err != nil {
		panic(err)
	}
	assert.FileExists(t, fsRoot.FullPath(dstMkv))
	assert.True(t, fsRoot.FileReplaced(dstMkv))
	for _, f := range dstExisting {
		assert.NoFileExists(t, fsRoot.FullPath(f))
	}
}

func TestUnsupportedSeriesTagsDoNothing(t *testing.T) {
	testCases := []struct {
		tag  string
		desc string
	}{
		{"", "empty tag"},
		{"foo", "ignored tag"},
		{fmt.Sprintf("%s_dst%ssrc", handler.TagPrefix[:len(handler.TagPrefix)-1], handler.TagSeparator), "missing tag separator"},
		{fmt.Sprintf("%s %sdst%[2]ssrc", handler.TagPrefix[:len(handler.TagPrefix)-1], handler.TagSeparator), "prefix with whitespace before separator"},
		{fmt.Sprintf("foo%sdst%[2]ssrc", handler.TagPrefix, handler.TagPrefix), "chars before the prefix"},
	}

	const dstRoot = "dst/series"
	const srcMkv = "src/series/S01E01.mkv"
	const dstMkv = "dst/series/S01E01.mkv"

	fsRoot := fs.New(dstRoot, nil, []string{srcMkv})
	defer fsRoot.Destroy()

	for _, tc := range testCases {
		e, err := parser.NewWithEnv(sonarr.NewEnv(false, fsRoot.FullPath(srcMkv), []string{tc.tag}, nil)).Parse()
		if err != nil {
			panic(err)
		}

		t.Run(tc.desc, func(t *testing.T) {
			sut := handler.New()
			if err := sut.Run(e); err != nil {
				panic(err)
			}
			assert.NoFileExists(t, fsRoot.FullPath(dstMkv))
		})
	}
}
