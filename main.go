package main

import (
	"flag"
	"os"
	"strings"

	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/duplicatarr/internal/handler"
	"gitlab.com/ddb_db/duplicatarr/internal/parser"
)

func main() {
	pretend := new(bool)
	config(pretend)
	v, ok := os.LookupEnv("sonarr_eventtype")
	if !ok || strings.ToLower(v) != "download" {
		wlog.Warningf("duplicatarr: unsupported event ignored [%s]", v)
		os.Exit(0)
	}

	var e parser.Event
	var err error
	if e, err = parser.New().Parse(); err != nil {
		panic(err)
	}

	var h handler.Handler
	if !*pretend {
		h = handler.New()
	} else {
		h = handler.NewDryRun()
	}
	if err := h.Run(e); err != nil {
		panic(err)
	}
}

func config(pretend *bool) {
	flag.BoolVar(pretend, "dry-run", false, "when true, don't do any actual copying or deleting, just log what would have been done")
	lvl := flag.String("log-level", "info", "log level (debug|info|warn|error)")
	flag.Parse()

	log := wlog.DefaultLogger()
	switch strings.ToLower(*lvl) {
	case "debug":
		log.SetLogLevel(wlog.Dbg)
	case "warn":
		log.SetLogLevel(wlog.Wrn)
	case "error":
		log.SetLogLevel(wlog.Err)
	default:
		log.SetLogLevel(wlog.Nfo)
	}
}
